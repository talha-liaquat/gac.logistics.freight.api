﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gac.Logistics.Freight.Api.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Gac.Logistics.Freight.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PurchaseOrdersController : ControllerBase
    {

        // POST: api/PurchaseOrders
        [HttpPost]
        [ProducesResponseType(typeof(PurchaseOrder), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Post([FromBody] List<PurchaseOrder> purchaseOrders)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok();
        }
    }
}
