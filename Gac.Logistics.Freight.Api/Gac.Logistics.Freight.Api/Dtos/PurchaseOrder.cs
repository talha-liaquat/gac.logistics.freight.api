﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Gac.Logistics.Freight.Api.Dtos
{
    public class PurchaseOrder
    {
        public string CorrelationId { get; set; }
        [Required]
        public string PoNumber { get; set; }

        [Required]
        public DateTime? PODate { get; set; }

        public Party Supplier { get; set; }


        public string ChangeOrderBatch { get; set; }

        public string ProcessInstance { get; set; }

        public string ShipToId { get; set; }

        /// <summary>
        /// ModeOfShipment
        /// </summary>
        public string Priority { get; set; }

        /// <summary>
        /// Freight Term
        /// </summary>
        public string IncoTermCode { get; set; }

        public DateTime? RequiredAt { get; set; }

        public string RigDepartmentCode { get; set; }

        public string ProjectCode { get; set; }

        public string CustomerAccountingCode { get; set; }

        /// <summary>
        /// currency header
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Currency in local
        /// </summary>
        public string LocalCurrency { get; set; }

        /// <summary>
        /// Exchange Rate
        /// </summary>
        public decimal? ExchangeRate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TermsOfPayment { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DocumentType { get; set; }


        /// <summary>
        /// PO Lines
        /// </summary>
        public List<PurchaseOrderLineItem> Items { get; set; }
    }
}
