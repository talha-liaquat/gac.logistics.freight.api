﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Gac.Logistics.Freight.Api.Dtos
{
    public class PurchaseOrderLineItem
    {
        public string CurrencyCode { get; set; }

        [Required]
        public string LineNumber { get; set; }

        [Required]
        public string PartNumber { get; set; }

        public string PartName { get; set; }

        public string PartLocalDesc { get; set; }

        public string UnitOfMeasure { get; set; }

        public string ManufacturingName { get; set; }

        public string ManufacturingModelName { get; set; }

        public string EnccNumber { get; set; }

        public string Status { get; set; }

        public string Active { get; set; }

        public string Priority { get; set; }

        public float PoQty { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal? TotalPrice { get; set; }

        public string CustomerVendorId { get; set; }

        public string CustomerVendorLocation { get; set; }

        public string CustomerMaterialId { get; set; }
    }
}
