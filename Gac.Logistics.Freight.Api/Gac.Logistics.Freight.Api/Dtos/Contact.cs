﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gac.Logistics.Freight.Api.Dtos
{
    public class Contact
    {
        public string Name { get; set; }

        public string Phone { get; set; }
    }
}
